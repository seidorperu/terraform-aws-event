variable "owner" {
  description = "Propietario del proyecto"
  type        = string
}

variable "project" {
  description = "Nombre del proyecto"
  type        = string
}

variable "env" {
  description = "Entorno de despliegue"
  type        = string
}
variable "is_enabled" {
  description = "Enable"
  default     = true
}
variable "description" {
  description = "Detalles del descripcion"
  type = string
  default = null
}
variable "arn_resource" {
  description = "Recurso a atachar"
  type = string
}
variable "schedule_expression" {
  description = "Time Exec"
  type = string
}
variable "input" {
  description = "Time Exec"
  type = string
  default = null
}