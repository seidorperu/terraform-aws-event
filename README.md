# AWS CloudWatch Event Rule module
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Terraform module which creates CloudWatch Event Rule resource on AWS, compatible Terraform >= v0.12.1 .

These types of resources are supported:

* [Event](https://www.terraform.io/docs/providers/aws/r/cloudwatch_event_rule.html)


## Usage

```hcl
module "aws_iam_event" {
    source                 = "git::ssh://git@bitbucket.org:seidorperu/terraform-aws-event.git"
    owner                  = "empresa"
    project                = "lambda"
    env                    = "dev"
    arn_resource           = "arn_resource"
    schedule_expression    = "0 20 * * ? *"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| owner | Propietario del proyecto. | map | n/a | yes |
| project | Nombre del proyecto. | map | n/a | yes |
| env | Entorno de despliegue. | map | n/a | yes |
| arn_resource | ARN del rescurso asociar | map | n/a | yes |
| schedule_expression | horario de ejecución | map | n/a | yes |


## Outputs

| Name | Description |
|------|-------------|
| arn | El ARN asignado por AWS a esta política.. |


## Authors

Seidor 

## License

Apache 2 Licensed. See LICENSE for full details.