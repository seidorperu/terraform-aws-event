terraform {
  required_version = ">= 0.12.1"
}
resource "aws_cloudwatch_event_rule" "this" {
  name                = "${var.owner}-${var.env}-${var.project}"
  description         = "${var.description == null ? null : var.description}"
  is_enabled          = "${var.is_enabled}"
  schedule_expression = "${length(var.schedule_expression) <= 2 ? "rate(${var.schedule_expression} days)" : "cron(${var.schedule_expression})"}"
}

resource "aws_cloudwatch_event_target" "this" {
  rule       = "${aws_cloudwatch_event_rule.this.name}"
  arn        = "${var.arn_resource}"
  input      = "${var.input}"
}
